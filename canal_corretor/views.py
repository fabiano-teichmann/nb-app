from django.shortcuts import render, get_object_or_404, redirect
from .forms import CorretorForm, ImobiliariaForm, UserModelForm
from .models import *
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.utils import timezone
from django.http import HttpResponseRedirect
from django.contrib import messages
# FORMULÁRIO DE lOGIN E LOGAUT
def do_login(request):
    if request.method =='POST':
        user = authenticate(username=request.POST['username'], password=request.POST['password'])
        if user is not None:
            login(request, user)
            #redireciona para a pagina de entrada
            return redirect(portal)
    return render(request, 'canal_corretor/index.html')

def do_logout(request):
    logout(request)
    return redirect('login')
# FORMULÁRIO DE lOGIN E LOGAUT
"""
   PAGINA EMPREENDIMENTOS
"""
@login_required(login_url='/')
def portal(request):
    cats = Categoria.objects.all()
    slides = Slide.objects.order_by('ordem')
    context = {'cats': cats, 'slides': slides }
    return render (request, 'canal_corretor/cat_list.html', context)

@login_required(login_url='/')
def pronto(request):
    eps = Empreendimento.objects.filter(status=4)
    return render(request, 'canal_corretor/ep_list.html',{'eps': eps})

@login_required(login_url='/')
def construcao(request):
    eps = Empreendimento.objects.filter(status=3)
    return render(request, 'canal_corretor/ep_list.html',{'eps': eps})

@login_required(login_url='/')
def lancamento(request):
    eps = Empreendimento.objects.filter(status=2)
    return render(request, 'canal_corretor/ep_list.html',{'eps': eps})

@login_required(login_url='/')
def em_breve(request):
    eps = Empreendimento.objects.filter(status=1)
    return render(request, 'canal_corretor/ep_list.html',{'eps': eps})

@login_required(login_url='/')
def ep_detail(request, pk):
    eps = get_object_or_404(Empreendimento, pk=pk)
    ptas = eps.planta.all()
    if len(ptas) == 0:
        ptas ="Ainda não temos conteúdo disponível "
    tbs = eps.tabela.all()
    imgs = eps.image.all()
    vds = eps.video.all()
    mts = eps.material.all()
    context =  { 'eps': eps, 'ptas': ptas, 'tbs': tbs, 'imgs': imgs, 'vds': vds, 'mts': mts}
    return render(request, 'canal_corretor/detail.html', context)

@login_required(login_url='/')
#categoria detail é similiar a ep_list
def cat_detail(request, pk):
    cat = get_object_or_404(Categoria, pk=pk)
    eps = cat.empreendimento.all()
    context = {'cat': cat, 'eps': eps}
    return render(request, 'canal_corretor/cat_detail.html', context)
"""
   PAGINA EMPREENDIMENTOS
"""

# FORMULÁRIOS DE CADASTRO

def cadastro(request):
    form = UserModelForm(request.POST or None)
    context = {'form': form}
    if form.is_valid():
        #instance = form.save(commit=False)
        user = form.save()
        user.set_password(user.password)
        user.save()

        #form = form.cleaned_data.get('username')
        return redirect(corretor_new)
 
    return render(request, 'canal_corretor/cadastro.html', context )


def imobiliaria_new(request):
    form = ImobiliariaForm(request.POST or None)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        instance = form.cleaned_data.get('razao')
        # tenho que  validar consultando se o corretor já não esta cadastrado
        return redirect(portal)
    return render (request, 'canal_corretor/cad_imobi.html', {'form':form}) 

# CORRETOR AFILIADO

#deferia ser uma classe protegida
def afiliado_new(request):
    form = AfiliadoForm(request.POST or None)
      #pega os dados do formulário
    context = {'form': form }
    if form.is_valid() :

        instance.save()
        #instance = form.clean_data.get('nome')
    else:
        messages.error(request,'Preencha todos os campos  obrigatórios.')
    
    return render(request, 'canal_corretor/afiliado_new.html', context) 

# Cadastro de corretor 
'''def corretor_new(request):
    form = CorretorForm(request.POST or None)
    user = UserModelForm(request.POST or None)
    context = {'form': form, 'user': user}
    if form.is_valid() and user.is_valid():
        #instance = form.save(commit=False)
        user.save()
        form.save()
    else:
        messages.error(request, 'Please correct the error below.')   
    return render(request, "canal_corretor/corretor.html", context)


'''
def corretor_new(request):
    form = CorretorForm(request.POST or None)
    context = {'form': form}
    if request.method == "POST":
        if form.is_valid():
            instance = form.save(commit=False)
            instance.save()
            instance = form.cleaned_data.get('nome')
            #  remcaminha para criar usuario e senha
            #return redirect(cadastro)
            return redirect(cadastro)
    return render(request, 'canal_corretor/cad_corretor.html', context)



 # PROVA
 #esperar para ver que tipo de prova
@login_required
def prova(request, pk):

    form = ProvaForm(request.POST or None)
    #provas = get_object_or_404(Prova, pk=pk)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        instance = form.clearned_data.get('pergunta')
        return redirect(portal)
    return render(request, 'canal_corretor/prova.html',{'form': form})
# PAGINA DE ENTRATADA DO PORTAL DO CORRETOR


#Venda



