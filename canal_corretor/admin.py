from django.contrib import admin
from .models import *

class EmpreendimentoAdmin(admin.ModelAdmin):
    save_on_top = True
    list_display = ('titulo', 'status', 'cat' )
    list_filter = ('titulo', 'status', 'cat')
    search_fields = ['titulo','status']
class MetaEmpreendimento:
    model = Empreendimento
    admin.site.register(Empreendimento, EmpreendimentoAdmin)

#imobilária

class ImobiliariaAdmin(admin.ModelAdmin):
    save_on_top = True
    list_display = ('razao', 'nome', 'cidade')
    list_filter = ('razao', 'nome', 'cidade','estado')
    search_fields = ['razao', 'nome', 'cidade','estado']
class MetaImobiliaria:
    model = Imobiliaria
    admin.site.register(Imobiliaria, ImobiliariaAdmin)

# Corretor Afiliado 
"""class AfiliadoAdmin(admin.ModelAdmin):
    save_on_top = True
class MetaCorretor:
    model = Afiliado
    admin.site.register(Afiliado, AfiliadoAdmin)
"""
#Corretor
class CorretorAdmin(admin.ModelAdmin):
    save_on_top = True
    list_display = ('nome', 'cidade', 'estado')
    list_filter = ('nome', 'cidade')
    search_fields = [ 'cidade', 'estado', 'nome']
class MetaCorretor:
    model = Corretor
    admin.site.register(Corretor, CorretorAdmin)

#categoria
class CategoriaAdmin(admin.ModelAdmin):
    save_on_top = True


class MetaCategoria(admin.ModelAdmin):
    model = Categoria
    admin.site.register(Categoria, CategoriaAdmin)
    list_filter = ('ep_id','nome')
    list_display = ('ep_id', 'nome')
#MATERIAL
class MaterialAdmin(admin.ModelAdmin):
    save_on_top = True
    list_display = ('ep_id', 'nome')
    list_filter = ('ep_id','nome')

class MetaMaterial:
    model = Material
    admin.site.register(Material, MaterialAdmin)

class ImageAdmin(admin.ModelAdmin):
    save_on_top = True
    list_filter = ('ep_id','nome') 
    list_display = ('ep_id', 'nome')
class MetaImage:
    model = Image
    admin.site.register(Image, ImageAdmin)

class TabelaAdmin(admin.ModelAdmin):
    save_on_top = True
    list_filter = ('ep_id','nome')
    list_display = ('ep_id', 'nome')
class MetaTabela:
    model = Tabela
    admin.site.register(Tabela, TabelaAdmin)

class PlantaAdmin(admin.ModelAdmin):
    save_on_top = True
    list_filter = ('ep_id','nome')
    list_display = ('ep_id', 'nome')
class MetaPlanta:
    model = Planta
    admin.site.register(Planta, PlantaAdmin)


class VideoAdmin(admin.ModelAdmin):
    save_on_top = True
    list_filter = ('ep_id','nome')
    list_display = ('ep_id', 'nome')

class MetaVideo:
    model = Video
    admin.site.register(Video, VideoAdmin)

class SlideAdmin(admin.ModelAdmin):
    save_on_top = True
    list_display = ('titulo', 'link')
    list_filter = ('titulo', 'link')
class MetaSlide:
    model = Slide
    admin.site.register(Slide, SlideAdmin)


