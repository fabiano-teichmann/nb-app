﻿from django.db import models
from django.utils import timezone
import datetime
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

estado = (
	('AC','Acre' ),
	('AL','Alagoas'),
	('AP','Amapá'),
	('AM','Amazonas'),
	('BA','Bahia'),
	('CE','Ceará'),
	('DF','Distrito Federal'),
	('ES','Espírito Santo'),
	('GO', 'Goiás'),
	('MA', 'Maranhão'),
	('MT', 'Mato Grosso'),
	('MS', 'Mato Grosso do Sul'),
	('MG', 'Minas Gerais'),
	('PA', 'Pará'),
	('PB', 'Paraíba'),
	('PR','Paraná' ),
	('PE','Pernambuco'),
	('PI', 'Piauí'),
	('RJ', 'Rio de Janeiro'),
	('RN', 'Rio Grande do Norte'),
	('RS', 'Rio Grande do Sul'),
	('RO', 'Rondônia'),
	('RR', 'Roraima'),
	('SC','Santa Catarina'),
	('São Paulo', 'SP'),
	('SE', 'Sergipe'),
	('TO', 'Tocantins'),
	)
'''uf = (
    ('1','AC' ),
    ('2','AL'),
    ('3','AP'),
    ('4','AM'),
    ('5', 'BA'),
    ('6','CE'),
    ('7', 'DF'),
    ('8','ES'),
    ('9','GO'),
    ('10','MA'),
    ('11','MT'),
    ('12','MS'),
    ('13','MG'),
    ('14','PA'),
    ('15','PB'),
    ('16','PR' ),
    ('17','PE'),
    ('18','PI'),
    ('19','RJ'),
    ('20','RN'),
    ('21','RS'),
    ('22','RO'),
    ('23','RR'),
    ('25','SC'),
    ('26','SP'),
    ('27','SE'),
    ('28','TO'),
    )
'''
status = (
	('1', 'Em Breve'),
	('2', 'Lançamento'),
	('3', 'Em construção'),
	('4', 'Pronto Para Construir'),
	)

escolha = (
	('1','a'),
	('2', 'b'),
	('3', 'c'),
	('4', 'd'),
	)
destaque = (
    ('1', 'Sim'),
    ('2', 'Não'),
)
ativo = (
    ('active', 'Primeiro Slide'),
    ('n', 'Não'),
    )
'''creci = (
    ('F', 'Físico'),
    ('J', 'Jurídico'),
    )
'''    
#CORRETOR AUTONOMO

class Corretor(models.Model):
    class Meta:
        verbose_name = "Corretore"

    nome = models.CharField(verbose_name='Nome*:',max_length=150, blank=False)
    nasc = models.DateField(verbose_name='Data de nascimento:')
    cpf = models.CharField(verbose_name='Cpf:', blank=False, max_length=16)
    creci =  models.CharField(verbose_name='Creci Físico*:', blank=False, max_length=12)
    tipo = models.CharField('Tipo', max_length=1, blank=False)
    uf = models.CharField('Estado',   max_length=2, blank=False)
    telefone = models.CharField(verbose_name='Telefone:', max_length=16, blank=True)
    phone = models.CharField(verbose_name='Celular:', max_length=16, blank=False)
    email = models.EmailField(max_length=254,verbose_name='E-mail:', blank=False)
    site = models.URLField(verbose_name='site', blank=True)
    endereco = models.CharField(max_length=254, verbose_name='Endereço *:',blank=False )
    bairro = models.CharField(verbose_name='Bairro*:',max_length=100,blank=False)
    cidade = models.CharField(verbose_name='Cidade*:',blank=False,max_length=200)
    cep = models.CharField(verbose_name=' Cep*:', blank=True, max_length=16)
    estado = models.CharField(max_length=100, choices=estado, blank=False, verbose_name='Estado*:' )
    create_date = models.DateTimeField('Data de criação',default=timezone.now)
    
    def __str__(self):
        return (self.nome)
@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

# CORRETOR LIGADO A IMOBILIARIA 



#IMOBILIARIA

class Imobiliaria(models.Model):
    img = models.ImageField(verbose_name='Insira uma imagem:',blank=True, upload_to='static/img/imob')
    razao = models.CharField(max_length=200 ,verbose_name=' Razão social  *:', blank=False)
    creci_j = models.CharField(max_length=6, verbose_name='Creci Jurídico *:', blank=False)
    tipo = models.CharField('Tipo', max_length=2, blank=False, null=True)
    uf = models.CharField('Estado', max_length=2, blank=False, null=True)
    nome = models.CharField(verbose_name='Nome Fantasia *:', blank=False, max_length=150)
    cnpj = models.CharField(verbose_name=' Cnpj *:', blank=False, max_length=30)
    endereco = models.CharField(max_length=254, verbose_name='Endereço *:',blank=False )
    bairro = models.CharField(verbose_name='Bairro *: ', max_length=100, blank=True)
    cidade = models.CharField(verbose_name='Cidade *: ',blank=False,max_length=200)
    cep = models.CharField(verbose_name=' Cep *: ', blank=False, max_length=12)
    estado = models.CharField(max_length=100, choices=estado, blank=False, verbose_name='Estado*:' )
    telefone = models.CharField(verbose_name='Telefone:', max_length=16, blank=True)
    phone = models.CharField(verbose_name='Celular: ', max_length=16, blank=True)
    email = models.EmailField(max_length=254,verbose_name='E-mail *:', blank=False)
    site = models.URLField(verbose_name='site', blank=True)
    resp = models.CharField(verbose_name='Corretor Resposável*:', blank=False, max_length=150)
    email_resp = models.CharField("Email do Responsável:", max_length=200)
    cpf = models.CharField(verbose_name='CPF *: ', blank=False, max_length=16)
    creci_f =  models.CharField(verbose_name=' Creci Físico *: ', blank=True, max_length=12)
    create_date = models.DateTimeField('Data de criação',default=timezone.now)
    def __str__(self):
        return (self.nome)


#CATEGORIA

class Categoria(models.Model):
    titulo = models.CharField(verbose_name='Nome:',max_length=100, blank=False)
    img = models.ImageField(verbose_name='Insira uma imagem',blank=True, upload_to='static/img/cat')
    descricao = models.TextField('Descrição', blank=True)

    def __str__(self):
        return (self.titulo)

#EMPREENDIMENTOS

class Empreendimento(models.Model):

    titulo = models.CharField('Nome:(campo obrigatório)', max_length=200, blank=False)
    sub_titulo= models.CharField('Sub titulo',blank=False, max_length=150)
    descricao = models.TextField('Descrição')
    img = models.ImageField(verbose_name='Insira uma imagem:',blank=True, upload_to='static/img/empreendimento')
    lancamento = models.DateField('Data prevista para lançamento',blank=True, null=True)
    cat = models.ForeignKey("Categoria",verbose_name='categoria', on_delete=models.PROTECT,related_name="empreendimento")
    estado = models.CharField(verbose_name='Estado: (campo obrigatório)',max_length=100, choices=estado, blank=True)
    status = models.CharField('Status', max_length=30, blank=False, choices=status)
    destaque = models.CharField('Destaque', max_length=3, blank=True, null=True, choices=destaque)

    def publish(self):
        self.lancamento = timezone.now()
        self.save()

    def __str__(self):
        return (self.titulo)


# MATERIAL DE APOIO DO CORRETOR

class Pagina(models.Model):
    titulo = models.CharField('Titulo',max_length=100, blank=False )
    sub_titulo = models.CharField('Subtitulo', max_length=150, blank=False)
    nomeimg = models.CharField('Descrição da imagem', max_length=100, blank=False)
    img = models.ImageField('Banner', blank=False, upload_to='static/img/')
    descricacao = models.TextField('Descrição', blank=False)

    def __str__(self):
        return(self.titulo)

class Slide(models.Model):
    titulo = models.CharField('Titulo',max_length=80, blank=False )
    img = models.ImageField('Imagem', blank=False, upload_to='static/slide')
    link = models.URLField('Link', blank=False)
    ordem = models.IntegerField('Ordem de Exibição', blank=False)
    active = models.CharField('1 ser exibido', choices=ativo, max_length=25)
    def __str__(self):
        return (self.titulo)


class Image(models.Model):
    nome = models.CharField(max_length=100,verbose_name='Nome do arquivo', blank=False)
    file = models.FileField(verbose_name='Insira uma imagem',blank=True, upload_to='static/upload/img')
    ep_id = models.ForeignKey("Empreendimento",related_name='image', on_delete=models.CASCADE)
 

    def __str__(self):
        return(self.nome)


class Tabela(models.Model):
    nome = models.CharField(max_length=100,verbose_name='Nome do arquivo', blank=False)
    file = models.FileField(upload_to='static/upload/doc',verbose_name='Arquivos ',blank=True)
    ep_id = models.ForeignKey("Empreendimento",related_name='tabela', on_delete=models.CASCADE)
    def __str__(self):
        return(self.nome)


class Planta(models.Model):
    nome = models.CharField(max_length=100,verbose_name='Nome do arquivo', blank=False)
    file = models.FileField(upload_to='static/upload/planta',verbose_name='Arquivos ',blank=True)
    ep_id = models.ForeignKey("Empreendimento", on_delete=models.CASCADE,related_name='planta')
    def __str__(self):
        return(self.nome)

class Material(models.Model):
	class Meta:
            verbose_name = 'Material de apoio'

	nome = models.CharField(max_length=100,verbose_name='Nome do arquivo', blank=False)
	file = models.FileField(verbose_name='Insira uma imagem',blank=True, upload_to='static/upload/material_apoio')
	ep_id = models.ForeignKey("Empreendimento",related_name='material', on_delete=models.CASCADE)
	def __str__(self):
		return(self.nome)
class Video(models.Model):
    iframe = models.URLField(max_length=250, verbose_name='Insira iframe', blank=False)
    nome = models.CharField(max_length=100,verbose_name='Nome do arquivo', blank=False)
    ep_id = models.ForeignKey("Empreendimento", related_name="video", on_delete=models.CASCADE)
    def __str__(self):
        return(self.nome)


class Prova(models.Model):
    nome = models.CharField(max_length=100, verbose_name='Nome da prova', blank=True)
    pergunta = models.CharField(max_length=200, verbose_name="Pergunta", blank=False)
    resposta = models.CharField(verbose_name='Resposta Correta',max_length=5, choices=escolha, blank=True)

    def __str__(self):
        return (self.nome)
